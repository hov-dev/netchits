<title>NetChits</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta property="og:title" content="Netchits - Multiple Social Network" />
<meta property="og:description" content="Netchits - Listen Music / Work / Follow with friends" />
<meta property="og:url" content="https://www.netchits.com" />
<meta property="og:tyoe" content="article" />

<meta property="og:image" content="https://www.netchits.com/images/opg/opgimage.png" />
<meta property="og:image:type" content="image/png" />

<meta property="og:locale" content="en_US" />
<meta property="og:site_name" content="NetChits" />
<meta name="author" content="Afgan Khalilov" />
<meta name="copyright" content="Afgan Khalilov" />
<meta name="keywords" content="social network, listen music, netchits, add friends, follow friends, NetChits, multiple social network" />



<!-- FavIcons -->




<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/favicon/apple-touch-icon.png') }}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('images/favicon/favicon-32x32.png') }}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon/favicon-16x16.png') }}">
<link rel="manifest" href="{{ asset('images/favicon/site.webmanifest') }}">
<link rel="mask-icon" href="{{ asset('images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">








<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
<script src="{{ asset('/js/app.js') }}"></script>
<script src="https://use.fontawesome.com/cf9930871a.js"></script>
