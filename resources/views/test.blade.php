















Здравствуйте, благодарю за отзыв.

Вот несколько работ из моего портфолио
- https://insure.az/en
- https://www.netchits.com/
- https://play.google.com/store/apps/details?id=agroup.com.healthmobapp

Вот краткая информация обо мне.

Для начала извиняюсь за ответ в столь поздний час, сами понимаете,
программисты иногда работают по ночам :)

Профессиональный опыт разработки 2 года.
PHP знаю хорошо, и пишу на нем каждый день.
Помимо Symfony также использую фреймворк Laravel.
C PostqreSQL знаком, Redis пока не приходилось использовать.

Вот краткий список остального.
- Mysql / phpmyadmin
- Linux / Git
- Html / Css / Bootstrap
- JavaScript / Jquery / Vue.js
- 1С-Bitrix / Wordpress / Opencard

Тестовое задание готов выполнить на выходных.
Всего доброго Кристина.
